//
// B A C K E N D _ S E T U P
//
// NOTES:
//
// *** Morgan ***
//
// Format parameter: 'dev'
// Concise output colored by response status for development use.
// The :status token will be colored red for server error codes,
// yellow for client error codes, cyan for redirection codes,
// and uncolored for all other codes.
//
// :method :url :status :response-time ms - :res[content-length]
//
//
// *** API ***
//
// API routes should always use nouns (főnevek) as resource identifiers
//
// POST /user or PUT /user:/id to create a new user,
// GET /user to retrieve a list of users,
// GET /user/:id to retrieve a user,
// PATCH /user/:id to modify an existing user record,
// DELETE /user/:id to remove a user.
//

//
// Init variables
//

//
// Create our app w/ express
//
const express  = require('express'),
	  app      = express(),
	  //
	  // process.env.PORT lets the port be set by Heroku
	  //
	  envPath = (app.settings.env == 'development') ? '/build' : '/dist',
	  port = process.env.PORT || 8001,
	  appDir = __dirname + envPath,
	  isDev = (app.settings.env == 'development') ? true : false,
	  mime = require('mime'),
	  developer = {

	  	browserSync: require('browser-sync').create(),
	  	notifier: require('node-notifier'),

	  };

if(isDev){

	developer.browserSync.init(['build/**/**.**'],{
		proxy: 'localhost:'+port,
		port: 8000,
		notify: true,
	});

}

var notify = (message) => {

	if(isDev){
		developer.notifier.notify({
			title: 'YOARTHFU NODE SERVER',
			message: message,
			icon: 'app/favicon.png',
			sound: true,
			wait: true
		});
	}
}

notify('Server application initialized.');

//var redis = require('redis');
//var client = redis.createClient(); //creates a new client


//
// Mongoose for mongodb
//

var mongoURI = {
		remote: 'mongodb://heroku_6rm9pd9s:8vd7lcf3echblmc263torc0m5i@ds143542.mlab.com:43542/heroku_6rm9pd9s',
		local: 'mongodb://localhost:27017/portfolio'
	}

var mongoose = require('mongoose');
mongoose.connect(mongoURI.remote,{ useMongoClient: true });

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {

	notify('Mongoose CONNECTED');
	//channel = new channels({name: 'test channel'});

	//content = new contents({title: 'test content', 'channel_id': channel._id});
});


//
// Log requests to the console (express4)
//
//var morgan = require('morgan');

//
// ERROR CODE SETUP
//
function statusCodes(res){

	res.status(500).send({error: 'Internal server error happened'});

}
//GIT

//var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)


//app.use(morgan('dev'));

app.get('/lic', (req, res) => {
//	console.log(res);
	console.log('contents requested');
	//notifier.notify('Quotes post sent from Client');

	//var result = mongoose.getCollection('contents').find({})
	res.send('1');

});

const getSchema = {

	//
	// Creating Mongo Model properly, before using it.
	// So we can load & save properly
	//
	channels: mongoose.model('channels', mongoose.Schema({
			body: String,
			description: String,
			label: String,
			name: String,
			renderPosition: Number,
	})),
	contents: mongoose.model('contents', mongoose.Schema({
			channel_id: String,
			title: String,
	}))
}
const getChannels = (res,id) => {

	this.channels = getSchema.channels;

	this.query = {
		//name: 'code',
		//renderPosition: { $gt: 1, $lt: 3 }
	}
	if(mongoose.Types.ObjectId.isValid(id)){
		console.log(id);
		this.query._id = id;
	}

	//
	// fields = .select(string);
	//

	this.fields = '_id name label description body';

	this.channels.find(this.query, this.fields, (err, channels) => {

		if(err){
			console.log(err);
		}

		res.json(channels);

	});

/*
	contents = mongoose.model('contents', mongoose.Schema({
		channel_id: { type: mongoose.Schema.Types.ObjectId, ref: 'channels' },
		title: String,
	}));
*/

}

const getContents = (res,id) => {

	this.contents = getSchema.contents;

	this.query = {
		//name: 'code',
		//renderPosition: { $gt: 1, $lt: 3 }
	}
	if(mongoose.Types.ObjectId.isValid(id)){
		this.query._id = id;
	}
	this.channel_id = id ? id.split(':') : 0;

	if(mongoose.Types.ObjectId.isValid(this.channel_id[1])){
		this.query.channel_id = this.channel_id[1];
		console.log(this.query);
	}
	//
	// fields = .select(string);
	//
	this.fields = '_id channel_id title';

	this.contents.find(this.query, this.fields, (err, channels) => {

		if(err){
			console.log(err);
		}

		res.json(channels);

	});

/*
	contents = mongoose.model('contents', mongoose.Schema({
		channel_id: { type: mongoose.Schema.Types.ObjectId, ref: 'channels' },
		title: String,
	}));
*/

}



const setHeaders = ()=> {
	console.log(req.originalUrl);

}

app.get('/contents/:id', (req, res) => {

	const id = req.params.id || 0;
	console.log(id);
	getContents(res, id);

});
app.get('/contents/', (req, res) => {

	getContents(res);

});

app.get('/channels/', (req, res) => {

	getChannels(res);

});
app.get('/channels/:id', (req, res) => {

	const id = req.params.id || 0;
	getChannels(res, id);

});




app.use(express.static(appDir));

app.use(function (req, res, next) {
	res.status(404);
	//this.type = mime.lookup(req.originalUrl);
	//console.log(this.type + ' ', Date.now())
	//res.type(this.type);

	res.status(404).send('ohh');

	//next()
})

//
// Listen (start app with node server.js) ======================================
//
app.listen(port);
console.log("Node APP listening in '"+appDir+"', on port "+ port);
