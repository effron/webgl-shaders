'use strict';

/* file paths */
const gulp = require('gulp'), 
	shell = require('gulp-shell'), 
	nodemon = require('gulp-nodemon'), 
	webserver = require('gulp-webserver'),
	//pass arguments in/from terminal
    arg = require('yargs').argv,
	mainBowerFiles = require('main-bower-files'),
	plugins = require("gulp-load-plugins")({
	//	pattern: ['gulp-*', 'gulp.*', 'main-bower-files'],
	//	replaceString: /\bgulp[\-.]/
	});

//
// DEVELOPER WEBSERVER WITH LIVERELOAD FACILITY
//
// Development Paths
const pathDev = {
	js: ['app/assets/js/*.js', '!app/assets/js/vendor/', '!app/assets/js/lib/'],
	data: ['app/assets/data/*.json', 'app/assets/data/**/*.json'],
	css: ['app/assets/sass/*.scss', 'app/assets/sass/**/*.scss', '!app/assets/sass/lib/'],
	tempCss: 'app/assets/css/'
	}, 
	errorFlag = false, 
	src_path = [
		'app/assets/js/*.js', 
		'!app/assets/js/vendor/', 
		'!app/assets/js/lib/', 
		'!app/assets/js/templates.js'
	],
	// IF env in production (--prod) set isProd and sass/compass config
	isProd = arg.prod ? true : false,
    sassStyle = arg.prod ? 'compressed' : 'nested',
    sourceMap = arg.prod ? false : true,
    autoprefix_browsers = [
		'last 2 version',
		'safari >= 5',
		'ie >= 8',
		'opera >= 12.1',
		'ios >= 6',
		'ie_mob >= 10',
		'android >= 4',
		'bb >= 10'
	];



gulp.task('bower-js-files', function(){

	var js = mainBowerFiles('**/*.js');

	console.log(js);
	
	return gulp.src(js)
		.pipe(gulp.dest('app/assets/js/vendor'), function(){
			console.log('bower done');
		})

});

gulp.task('bower-css-files', function(){

	var css = mainBowerFiles('**/*.css');
	console.log(css);
	return gulp.src(css)
//		.pipe(plugins.filter('**/*.css'))
		.pipe(gulp.dest('app/assets/css/vendor'));
		
});

// Don't stop gulp if error is a warning (jenkins benefit)
gulp.task('jshint', () => {
	
	console.log(' DO JSHINT ');

	return gulp.src(src_path).pipe(plugins.jshint()).pipe(plugins.jshint.reporter('jshint-stylish')).pipe(plugins.jshint.reporter('default'))
	//Stop gulp only if error but keep working if is a warning.
	.pipe(map((file, cb) => {

		if (!file.jshint.success) {
			file.jshint.results.forEach((err) => {
				const checkError = err.error.code
				if (checkError.charAt(0) === 'W') {
					//console.log('it is a warning')
				} else {
					//console.log('it is an Error')
					errorFlag = true;
				}
			});
		}

		cb(null, file);
	}))
	//.pipe(plugins.jshint.reporter('fail'))
	.pipe(plugins.size({
		title: 'Linting JS files'
	})).on('end', () => {
		if (errorFlag) {
			process.exit(1);
		}
	});
});

//CSS task. The concatenation and minification will be done in the html task
gulp.task('css', () => {
	console.log('DO CSS');
	return gulp.src(pathDev.css).pipe(plugins.compass({
		//config_file: './config.rb',
		css: 'app/assets/css/',
		sass: 'app/assets/sass/',
		output_style: sassStyle
	}))
	// If add on error this will keep compiling without stop on issue
	// .on('error', function(error){
	//   // console.log(error.message);
	//   console.log(error);
	//   this.emit('end');
	// })
	.pipe(plugins.autoprefixer({
		browsers: autoprefix_browsers
	})).pipe(gulp.dest(pathDev.tempCss)).pipe(plugins.size({
		title: 'Compiled SASS and saved in app -> CSS folder'
	}));
});


//Task to watch changes on folders and file. 
gulp.task('watch', () => {
	console.log('Watch services');
	gulp.watch(['app/*.html']);
	gulp.watch(pathDev.js, ['jshint']);
	gulp.watch(pathDev.css, ['css']);
	gulp.watch(pathDev.data);
});

//
// PRODUCTION SERVER for HEROKU
//
gulp.task('webserver', () => {

	// configure nodemon
	nodemon({
		// the script to run the app
		script: 'server.js',
		// this listens to changes in any of these files/routes and restarts the application
		watch: ["server.js"],
		ext: 'js'
		//
		// Below i'm using es6 arrow functions but you can 
		// remove the arrow and have it a normal 
		// .on('restart', function() { // then place your stuff in here }
		//
	}).on('restart', () => {
		// I've added notify, which displays a message on restart.
		// Was more for me to test so you can remove this
		//gulp.src('server.js').pipe(notify('Reloading Server JS'));
		console.log('Restarting ServerJS due to file change');
	});

});

gulp.task('bower-files', [
	'bower-js-files', 
	'bower-css-files'
], function(){
	console.log('Bower files compiled into dev');
});

gulp.task('serveprod', ['webserver']);
gulp.task('servedev', ['webserver','watch']);


gulp.task('build', ['webserver'], () => {
	return gulp.src('').pipe(shell(['node r.js -o app/assets/js/app.build.js']))
})