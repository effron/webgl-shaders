(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(factory((global.webglWidget = global.webglWidget || {})));
}(this, (function (exports) { 
	'use strict';
	
    var webglWidget, app = function(id) {
        app = this;
    };


	function Run(){
		
		console.log('WEBGL Widget process started.');
		
	}
	Run.prototype = {};

	Run.prototype.constructor = Run;

	function _import(lib){
		THREE = lib;
	}
	
	_import.prototype = {};
	
    app.prototype = {

		init: function(id){
			
			var container = document.getElementById(id) || document.body;
			
			app.w = container.offsetWidth || window.innerWidth;
			app.h = container.offsetHeight || window.innerHeight;

            app.scene = new THREE.Scene();
            app.rotateScene = false;
            
            app.camera = new THREE.PerspectiveCamera(75, app.w / app.h, 0.05, 1000);
            app.camera.position.set(0, 16, 34);

            app.renderer = new THREE.WebGLRenderer({
                alpha: true,
                antialias: true
            });
            app.renderer.shadowMap.enabled = false;
            app.renderer.shadowMap.type = THREE.PCFShadowMap;

            app.renderer.setClearColor(0x777777);
            app.renderer.setSize(app.w, app.h);

			//
            // SETUP FLOOR 
			//
			
            var noiseScale = 1.5;
            var size = 40;
            var height = 0.025;
			
			app.group = new THREE.Object3D();//create an empty container
			app.plantgroup = new THREE.Object3D();//create an empty container

            var floor = new THREE.Mesh(
                new THREE.ParametricGeometry(function(u, v) {
                    var x = u - 0.5;
                    var y = v - 0.5;
                    return new THREE.Vector3(x, y, noise.simplex2(x * noiseScale, y * noiseScale) * height);
                }, 24, 24),
                new THREE.MeshPhongMaterial({
                    side : THREE.DoubleSide,
                    color : 0xbbbbbb,
                    shininess : 40,
                    shading : THREE.FlatShading
                })
            );

            floor.position.set(0, 0.0, 0);
            floor.scale.set(size, size, size);
            floor.rotation.set(- Math.PI * 0.5, 0, 0);
            floor.castShadow = floor.receiveShadow = true;
            
            app.group.add(floor);
            		
			
			app.plant(new THREE.Vector3(0,0,0))

            var light0 = new THREE.DirectionalLight(0xffffff, 1.0);
            light0.position.set(20, 30, 20);
            light0.castShadow = true;
            light0.shadow.camera.fov = 120;
            light0.shadow.camera.near = 0.1;
            light0.shadow.camera.far = 1000;
            light0.shadow.camera.top = light0.shadow.camera.right = 20;
            light0.shadow.camera.bottom = light0.shadow.camera.left = -20;
            light0.shadow.mapSize.width = light0.shadow.mapSize.height = 2048;
            app.scene.add(light0);

            app.light = light0;

            //var light1 = new THREE.DirectionalLight(0xffffff, 0.5);
            //light1.position.set(-1, 1, -1);
            //app.scene.add(light1);

            var ambientLight = new THREE.AmbientLight(0x404040);
            app.scene.add(ambientLight);

			app.scene.add(app.group);
			
            app.clock = new THREE.Clock();
            app.controls = new THREE.OrbitControls(app.camera, app.renderer.domElement);
            app.controls.enablePan = false;
            app.controls.enableDamping = true;
            app.controls.dampingFactor = 0.5;
            app.controls.maxPolarAngle = Math.PI * 0.45;
            app.controls.minDistance = 7.0;
            app.controls.maxDistance = 40;

            app.mouse = new THREE.Vector2(-1, -1);

            var dragged = false;

            var clearDrag = function(e) {
                dragged = false;
            };

            var drag = function(e) {
                dragged = true;
            };

            var raycaster = new THREE.Raycaster();

            var click = function(e) {
                if(dragged) return;
                if(!app.treeMapTexture || !app.treeNormalMap) return;

                app.mouse.x = (e.clientX / window.innerWidth) * 2 - 1;
                app.mouse.y = - (e.clientY / window.innerHeight) * 2 + 1;
                raycaster.setFromCamera(app.mouse, app.camera);
                var intersect = raycaster.intersectObject(floor);
                if(intersect.length > 0) {
                    var point = intersect[0].point;
                    //app.plant(new THREE.Vector3(point.x, point.y, point.z));
                }
            };

            document.addEventListener('mousedown', clearDrag);
            document.addEventListener('mousemove', drag);
            document.addEventListener('mouseup', click);

            document.addEventListener('touchstart', clearDrag);
            document.addEventListener('touchmove', drag);
            document.addEventListener('touchend', click);
            
            window.addEventListener('resize', app.resize);
            

            container.append(app.renderer.domElement);

            app.loop();
			
		},
        resize : function() {

            app.camera.aspect = app.w / app.h;
            app.camera.updateProjectionMatrix();
            app.renderer.setSize(window.innerWidth, window.innerHeight);
        },		
		remoteRotate : function(){
			app.rotateScene = app.rotateScene ? false : true;
		},
		remotePlant : function(){

			var min = -15;
			var max = 15;
			// and the formula is:
			var x = Math.floor(Math.random() * (max - min + 1)) + min;
			var z = Math.floor(Math.random() * (max - min + 1)) + min;
			
			app.plant(new THREE.Vector3(x, 0, z));
			
		},
        plant : function(position) {

            var tree = new THREE.Tree({
                generations : 4,
                length : Math.random() * 3 + 2,
                uvLength : 16.0,
                radius : 0.5,
                radiusSegments : 8
            });
            var geometry = THREE.TreeGeometry.build(tree);
			 
            //var treeMat = new THREE.TreeMaterial();
            //treeMat.uniforms.uGrow.value = 0.0;
			
			var treeMat = new THREE.MeshPhongMaterial({
				color: 0x9a8268
			});//new THREE.TreeMaterial();

            var mesh = new THREE.Mesh(geometry, treeMat);
            //mesh.customDepthMaterial = depthMat;
            mesh.castShadow = mesh.receiveShadow = true;
            
            mesh.position.set(position.x, position.y, position.z);
            
            app.group.add(mesh);



        },
		
        loop : function() {
            
            requestAnimationFrame(function() { app.loop(); });

            //TWEEN.update();

            var delta = app.clock.getDelta();
            app.controls.update(delta);
			
			if(app.rotateScene){
				app.group.rotation.y += 0.01;
			}
            // var t = app.clock.elapsedTime * 0.1;
            // var x = Math.cos(t) * 20;
            // var z = Math.sin(t) * 20;
            // app. .position.set(x, 20, z);

            // app.renderer.render(app.scene, app.camera);
            app.renderer.render(app.scene, app.camera);
            
        },

		
	}

	//app.prototype.constructor = app;

	exports.Run = Run;
	exports.app = app;
	exports._import = _import;
	
})));