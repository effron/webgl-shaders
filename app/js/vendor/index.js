import angular from 'angular';
import requestFrame from 'request-frame';
import requestFrameModern from 'request-frame-modern';
//import TWEEN from 'tween.js';
import * as TWEEN from 'es6-tween';
import PIXI from 'pixi.js';

//try { delete $window.moment; } catch (e) {$window.moment = undefined;
// IE8 doesn't do delete of window vars
//}

class _TWEEN {
    constructor(){
        const module = TWEEN;
        delete window.TWEEN;
        console.log('%cTWEEN module: %cinitialized', 'font-family:monospace; color:#00ccff', 'color:#8aff00');
        return module;
    }
}
class _requestFrame {
    constructor(){
        const module = requestFrame;
        delete window.requestFrame;
        console.log('%crequestFrame module: %cinitialized', 'font-family:monospace; color:#00ccff', 'color:#8aff00');
        return module;
    }
}
class _requestFrameModern {
    constructor(){
        const module = requestFrameModern;
        delete window.requestFrameModern;
        console.log('%crequestFrameModern module: %cinitialized', 'font-family:monospace; color:#00ccff', 'color:#8aff00');
        return module;
    }
}
class _PIXI {
    constructor(){
        console.log('%cPIXI module: %cinitialized', 'font-family:monospace; color:#00ccff', 'color:#8aff00');
        const module = window.PIXI;
        delete window.PIXI;
        return module;
    }
}

let Vendors = angular.module('app.vendors', [])
.factory('TWEEN', () => new _TWEEN)
.factory('requestFrame', () => new _requestFrame)
.factory('requestFrameModern', () => new _requestFrameModern)
.factory('PIXI', () => new _PIXI);
export default Vendors;
