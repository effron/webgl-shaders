//
// STARTUP-SEQUENCE
//
// BOOTSTRAPS ANGULAR ONTO THE `window.document` NODE
// NOTE: THE `ng-app` ATTRIBUTE SHOULD NOT BE ON THE `index.html` WHEN USING `ng.bootstrap`
//
     
define([
    'require',
    'angular',
    'jquery',
    'app',
], function (require, ng) {
    'use strict';
    
    //
    // PLACE OPERATIONS THAT NEED TO INITIALIZE PRIOR TO APP START HERE
    // USING THE `run` FUNCTION ON THE TOP-LEVEL MODULE
    //
	
    require(['domReady!'], function (document) {
        ng.bootstrap(document, ['app']);
    });
});