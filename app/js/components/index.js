import angular from 'angular';

let componentsModule = angular.module('app.components', []);

import IntroComponent from './intro/intro.component';
componentsModule.component('intro', IntroComponent);

import AmbientComponent from './ambient/ambient.component';
componentsModule.component('ambient', AmbientComponent);

import ExploreComponent from './explore/explore.component';
componentsModule.component('explore', ExploreComponent);

export default componentsModule;
