/*
// Define the `intro` module
angular
	.module('ambient', [
//		'ngRoute'
	])
*/


define([
    'angular',
    'components/ambient/ambient.component'
], function (ng) {
    'use strict';

    return ng.module('com.ambient', ['services.graphics']);
});
		

