class IntroController {

	constructor($scope) {

		'ngInject';

		document.querySelectorAll('.logo');

		this.init($scope);

	}

	init($scope) {

		this.name = 'intro component'; //direct test variable

		var el = document.querySelectorAll('.welcome');

		document.addEventListener("DOMContentLoaded", function(event) {
			$scope.$apply(function() {
				$scope.images = true;
			});

		});

		$scope.$on('$viewContentLoaded', function(){

		});


	}

}

let IntroComponent = {
	bindings: {
		ngSrc: '='
	},
	controller: IntroController,
	templateUrl: 'components/intro/intro.template.html'
};

export default IntroComponent;
