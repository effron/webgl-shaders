define(['angular'], function(ng){
	
	'use strict';
	
	return ng.module('com.explore', ['services.graphics']);

});