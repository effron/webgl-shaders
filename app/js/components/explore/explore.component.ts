class ExploreController {

	constructor($scope){

		'ngInject';
		this.templateUrl = 'components/explore/explore.template.html';
		this.bindings = '=';
	}

	controller($scope, $http){

		'ngInject';
		
		//['$scope', '$routeParams', '$http', 'WebGLRenderer2D', function($scope, $routeParams, $http, gfx){
		var self = this;

		//gfx.foo = 'huhh...';

		//console.log(gfx);
		//$scope.imagePath = 'img/washedout.png';

		this.name = 'explore component'; //direct test variable
		//console.log(WebGLRenderer2D.remote);
		//WebGLRenderer2D.remote = 'modified';
		//console.log(WebGLRenderer2D.remote);
		//console.log(WebGLRenderer2D);
		$scope.loader = true;

		$scope.loaderMessage = "Processing data";

		console.log(this.name);
		$http.get('/channels/', {timeout:1000*10}).then(function(response){

			if(response.data){
				self.content = response.data;

				console.log('%c Data arrived', 'font-family:monospace; color:green');
	        	$scope.data = true;
				$scope.loader = false;
			} else {
				console.log('err');
				$scope.loaderMessage = "Unable to retreive data";
			}
		}, function(response){
	    	$scope.loaderMessage = "Communication lost with the cloud";
		});

	}

}

let ExploreComponent = new ExploreController;

export default ExploreComponent;
