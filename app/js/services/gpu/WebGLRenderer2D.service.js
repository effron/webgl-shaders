class WebGLRenderer2D {

	//
	// SERVICES
	// ARE
	// STRICTLY
	// DEFINED
	// AS
	// O B J E C T S
	//

	constructor(PIXI, requestFrame, TWEEN){

		'ngInject';
		//'pixi', 'request-frame', 'tween', 'pixi-filters',
		//'pixi-picture', 'lodash'], function(ng, Parallax, requestFrame, tween){

		// POLYGON" IS DERIVED FROM
		// THE GREEK POLUS (MANY)
      	// AND GONIA (ANGLE).

		//
		// Parallax engine setup
		//
		// - Console
		// - Renderer (AutoDetect)
		//
		console.log('why im i running?');
		PIXI.utils._saidHello = false;
		this.PIXI = PIXI;
		this.loader = PIXI.loader;
		this.renderer = new PIXI.WebGLRenderer(window.innerWidth,window.innerHeight);

		this.container = new PIXI.Container;
		this.stage = new PIXI.Container;
		this.Sprite = PIXI.Sprite;
		this.extras = {
			TilingSprite: PIXI.extras.TilingSprite
		}
		this.Text = PIXI.Text;
		this.filters = PIXI.filters;
		this.BLEND_MODES = PIXI.BLEND_MODES;
		this.Graphics = PIXI.Graphics;
		this.Point = PIXI.Point;

		this.renderer.backgroundColor = 0x000000;

		this.request = requestFrame('request');
		this.cancel = requestFrame('cancel');
		this.TWEEN = TWEEN;


		this.animation = {

			update: (time)=>{

				//sprite.position.y += (lFollowY - sprite.position.y) * 1/props.friction;
				this.animation.task.run(time);
				this.TWEEN.update(time);

				this.renderer.render(this.stage);
				this.task = requestFrame('request')(this.animation.update);

			},
			task: {

				add: function(id,task){

					this.pool[id]=task;
				},

				merge: function(id,task){
					var cache = this.pool[id];
					delete this.pool[id];
					this.pool[id]=angular.merge(cache, task);
					console.log('Merge: '+this.pool[id]);
				},
				remove: function(id){
					delete this.pool[id];
				},
				run: function(time){

					angular.forEach(this.pool, function(val, key) {
						//console.log('key:'+ key + ' | val: ' + val);
						val(time);
					});

				},
				pool: {}
			}

		};

		this.process = {

				init (){
					if (/*@cc_on!@*/false) { // check for Internet Explorer
						document.onfocusin = this.process.resume;
						document.onfocusout = this.process.suspend;
					} else {

						window.onfocus = this.resume;
						window.onblur = this.suspend;
					}
				},
				resume: () => {
					console.log('WebGL 2D process resumed');
					this.task = this.request(this.animation.update);
				},
				suspend: () => {
					console.log('WebGL 2D process suspended');
					this.cancel(this.task);
				},


		};

		//this.process.init(this.animation, this.request, this.cancel);

		//return this;

	}


	getTexture(resourceName){
		console.log(this.loader.resources[resourceName]);
		return this.loader.resources[resourceName].texture;
	}
	//
	// Perlin Noise function for tumbling and dust movement
	//

	perlinNoise(size){

		this.gx = [];
		this.gy = [];
		this.p = [];
		this.size = size || 256;


		for( var i = 0; i < this.size; i++ ) {
			this.gx.push( Math.random()*2-1 );
			this.gy.push( Math.random()*2-1 );
		}

		for( var j = 0; j < this.size; j++ ) {
			this.p.push( j );
		}
		this.p.sort(function() {return 0.5 - Math.random();});

		this.noise2 = ( x, y ) => {
			// Compute what gradients to use
			let qx0 = x | 0;
			let qx1 = qx0 + 1;
			let tx0 = x - qx0;
			let tx1 = tx0 - 1;

			let qy0 = y | 0;
			let qy1 = qy0 + 1;
			let ty0 = y - qy0;
			let ty1 = ty0 - 1;

			// Make sure we don't come outside the lookup table
			qx0 = qx0 % this.size;
			qx1 = qx1 % this.size;

			qy0 = qy0 % this.size;
			qy1 = qy1 % this.size;

			// Permutate values to get pseudo randomly chosen gradients
			let q00 = this.p[(qy0 + this.p[qx0]) % this.size];
			let q01 = this.p[(qy0 + this.p[qx1]) % this.size];

			let q10 = this.p[(qy1 + this.p[qx0]) % this.size];
			let q11 = this.p[(qy1 + this.p[qx1]) % this.size];

			// Compute the dotproduct between the vectors and the gradients
			const v00 = this.gx[q00]*tx0 + this.gy[q00]*ty0;
			const v01 = this.gx[q01]*tx1 + this.gy[q01]*ty0;

			const v10 = this.gx[q10]*tx0 + this.gy[q10]*ty1;
			const v11 = this.gx[q11]*tx1 + this.gy[q11]*ty1;

			// Modulate with the weight function
			const wx = (3 - 2*tx0)*tx0*tx0;
			const v0 = v00 - wx*(v00 - v01);
			const v1 = v10 - wx*(v10 - v11);

			const wy = (3 - 2*ty0)*ty0*ty0;
			const v = v0 - wy*(v0 - v1);

			return v;
		};
		this.modulate = (renderer, texture, x, y) => {

			var cw = renderer.width,
				ch = renderer.height,
				iw = texture.width,
				ih = texture.height;

			x = (x%iw - iw) % iw;
			y = (y%ih - ih) % ih;
			for( var nx = x; nx < cw; nx += iw ) {
				for( var ny = y; ny < ch; ny += ih ) {
					return {x: nx, y: ny};
				}
			}

		};


    }

	//
	// F I L T E R S
	//

	setFilter(){
		this.blur = function(props){
			this.filter = new PIXI.filters.BlurFilter();
			this.filter.blur = props.blur || 0;
			return this.filter;
		};
		this.displacement=function(props){
			//displacement = new Parallax.extras.TilingSprite(getTexture('dsp2'), renderer.width, renderer.height);
			this.filter = new PIXI.filters.DisplacementFilter(props.sprite);

			container.addChild(displacement);
			//this.sprite.scale.x = 100;

			//this.filter.scale.x = this.filter.scale.y = 40;

			//console.log(Parallax.filters.DisplacementFilter);
			return this.filter;
		};
		this.bloom = function(props){
			return new PIXI.filters.BloomFilter(props);
		};
	};


}
let _WebGLRenderer2D = function(){
	'ngInject';
	console.log('why am i running?');
	return {
		a:1
	}
}
export default WebGLRenderer2D;
