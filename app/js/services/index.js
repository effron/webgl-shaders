import angular from 'angular';

//
// SERVICES
//
// Create the module where our functionality can attach to
//
let Services = angular.module('app.services', []);

//
// Services
//
import WebGLRenderer2D from './gpu/WebGLRenderer2D.service';
Services.service('WebGLRenderer2D', WebGLRenderer2D);

export default Services;
