//define(['angular', 'pixi', 'request-frame', 'TWEEN', 'pixi-filters', 'pixi-picture', 'lodash'], function(ng, Parallax, requestFrame, TWEEN){
class RealParallaxDirective {

	constructor(WebGLRenderer2D, TWEEN, requestFrame, $location, $scope){

		'ngInject';
		//
		// No Angular DI available in the constructor method
		// Angular only injects dependencies into controller members {link, controller}
		// But, we created a standalone Class for THE CONTROLLER METHOD.
		//

		this.template = '';
        this.restrict = '';
		this.scope = {
			parallaxFriction: '@',
			parallaxScale: '@',
			parallaxTexture: '@',
		};
		this._scope = $scope;
		this.request = requestFrame('request');
		this.cancel = requestFrame('cancel');

		this._location = $location;
		//this.elem = $elem;
		this.WebGLRenderer2D = WebGLRenderer2D;
		this.TWEEN = TWEEN;
		// ---------------------------------------------------------------

		this.renderer = WebGLRenderer2D.renderer;
		this.requestFrame = requestFrame;

		this.modifiers = {i: 0, w: 0, h: 0, tumble: 0};
		this.mouse = {x: 0, y: 0, cx: 0, cy: 0};
		this.draw = {x: 0, y: 0, mx: 0, my: 0, dx: 0, dy:0};
		this.TRANSFORMATIONS={};
		this.lFollowY=0;
		this.lFollowX=0
		this.x=0;
		this.y=0;


		//
		// THROUGHOUT THE PROCESS MULTIPLE SIGNALS CAN BE DISPATCHED.
		//
		// loader.onProgress.add(() => {}); // called once per loaded/errored file
		// loader.onError.add(() => {}); // called once per errored file
		// loader.onLoad.add(() => {}); // called once per loaded file
		// loader.onComplete.add(() => {}); // called once when the queued resources all load.
		//


	}

	link($scope, $element){

		this._element = $element;
		this.props = {
			scale: ($scope.parallaxScale) ? $scope.parallaxScale : '1.1',
			friction: ($scope.parallaxFriction) ? $scope.parallaxFriction : 30,
			texture: $scope.parallaxTexture,
		};

		this.WebGLRenderer2D.loader
		.add('bg1', this.props.texture)
		.add('font', '/assets/fonts/Newtown.ttf')
		.add('dsp1', '/assets/images/displacementmap01.jpg')
		.add('dsp2', '/assets/images/displacementmap02.jpg')
		.add('dustNear', '/assets/images/background/dust-near.jpg')
		.add('dustFar', '/assets/images/background/dust-far.jpg')
		.add('vignette', '/assets/images/background/vignette.png')
		.add('texture01', '/assets/images/textures/scanbeam.jpg')
		.load((loader, resources) => {
			this.init(loader, resources, $scope);
		});

	}
	init(loader, resources, $scope){


		//
		// - Create a container object called the `stage`, that's the PIXI way
		// - ObjectContainer to be responsive
		// - Sprite for layer
		//
		let engine = this.WebGLRenderer2D;
		let renderer = this.renderer;
		let getTexture = this.WebGLRenderer2D.getTexture;

		this.stage = engine.stage;
		this.container = engine.container;

		//var screen = new Parallax.Sprite(null, renderer.width, renderer.height);

		this.sprite = new engine.Sprite(resources.bg1.texture, renderer.width, renderer.height);
		this.dustNear = new engine.PIXI.extras.TilingSprite(resources.dustNear.texture, renderer.width, renderer.height);
		this.dustNear2 = new engine.PIXI.extras.TilingSprite(resources.dustNear.texture, renderer.width, renderer.height);
		this.dustFar = new engine.PIXI.extras.TilingSprite(resources.dustFar.texture, renderer.width, renderer.height);
		this.vignette = new engine.PIXI.Sprite(resources.vignette.texture, renderer.width, renderer.height);
		this.vignette.width = renderer.width;
		this.vignette.height = renderer.height;

		this.texture = new engine.PIXI.extras.TilingSprite(resources.texture01.texture, renderer.width, renderer.height);

	    // Create some white text using the Snippet webfont.
		this.logo = new engine.Text("YOARTHFU",
			{
				fontFamily: "Newtown",
				fontSize: "60px",
				align: "center",
				fill: "white",
				fontWeight: 600,
				padding:20,
			});

		this.logo.x = renderer.width/2-this.logo.width/2;
		this.logo.y = renderer.height/2-this.logo.height/2;
		this.logo.alpha = 0;

		this.logo.filter = {
			//shockwave: new Parallax.filters.ShockwaveFilter(),
			blur: new engine.filters.BlurFilter()
		}
		this.logo.filters = Object.values(this.logo.filter);
		//const s = this.sprite;
		this.TRANSFORMATIONS = {
			//
			// MUST BE 1 DIMENSION
			//
			_default: {
				sprite:{
					alpha:0,
					scaleX:1,
					scaleY:1,
					pivotX:0,
					pivotY:0,
					rotation:0
				},
				texture:{
					alpha:1,
					scaleX:1.4,
					scaleY:1.4,
					pivotX:renderer.width/2,
					pivotY:renderer.height/2,
					positionX: renderer.width/2,
					positionY: renderer.height/2,
					rotation:0,
					tilePositionX:1024,
					tilePositionY:0,
				}
			},
			_inherit: {
				//
				// Inheritances must be executed
				//
				sprite: (()=>{
					return {
						alpha: this.sprite.alpha,
						scaleX:this.sprite.scale.x,
						scaleY:this.sprite.scale.y,
						pivotX:this.sprite.pivot.x,
						pivotY:this.sprite.pivot.y,
						rotation:this.sprite.rotation
					}
				})(),
				texture: (()=>{
					return {
						tilePositionX:this.texture.tilePosition.x,
						tilePositionY:this.texture.tilePosition.y
					}
				})()


			},
			explore: {
				sprite:{
					alpha:0.5,
					scaleX:1.4,
					scaleY:1.4,
					pivotX:100,
					pivotY:160,
					rotation:-0.1
				}
			}


		}

		//this.sprite.blendMode = Parallax.BLEND_MODES.NORMAL;
		this.sprite.cacheAsBitmap = true;


		this.texture.blendMode = engine.BLEND_MODES.MULTIPLY;
		this.texture.alpha = this.TRANSFORMATIONS._default.texture.alpha;
		this.texture.pivot.set(this.TRANSFORMATIONS._default.texture.pivotX,this.TRANSFORMATIONS._default.texture.pivotY);
		this.texture.position = {x: this.TRANSFORMATIONS._default.texture.positionX , y: this.TRANSFORMATIONS._default.texture.positionY }
		this.texture.scale = {x: this.TRANSFORMATIONS._default.texture.scaleX, y: this.TRANSFORMATIONS._default.texture.scaleY };

		//Parallax.CanvasTinter.getTintedTexture(this.sprite, '0x000000');
		let gfx = new engine.Graphics();

		//
		// Fill background color
		//
		gfx.beginFill(0x000000, 1)

		this.screen = gfx.drawRoundedRect(0, 0, renderer.width, renderer.height, 0);
		gfx.endFill();
		this.screen.alpha = 1;
		//displacement = new Parallax.extras.TilingSprite(getTexture('dsp1'), renderer.width, renderer.height);
		//Parallax.SCALE_MODES.DEFAULT = Parallax.SCALE_MODES.NEAREST;



		let sp = {x:this.sprite.width,y:this.sprite.height};
		let st = {x:renderer.view.width,y:renderer.view.height};
		let winratio = st.x/st.y;

		let spratio = sp.x/sp.y;
		let scale = 1;
		let pos = new engine.Point(0,0);

		if(winratio > spratio) {
			//photo is wider than background
			scale = st.x/sp.x;
			pos.y = -((sp.y*scale)-st.y)/2;
		} else {
			//photo is taller than background
			scale = st.y/sp.y;
			pos.x = -((sp.x*scale)-st.x)/2;
		}

		let noise = new this.WebGLRenderer2D.perlinNoise();

		this.modifiers.tumble = 32;
		this.modifiers.i = 1563;

		const cw = renderer.width + this.modifiers.tumble * 4;
		const ch = renderer.height + this.modifiers.tumble * 2;
		const image = this.sprite.texture;
		// Aspect zoom
		this.modifiers.w = cw;
		this.modifiers.h = Math.ceil(image.height / (image.width / cw ));
		if( this.modifiers.h < renderer.height ) {
			this.modifiers.w = Math.ceil(image.width / (image.height / ch ));
			this.modifiers.h = ch;
		}

		this.sprite.width = this.modifiers.w;
		this.sprite.height = this.modifiers.h;

		//
		// BUILT-IN BLEND-MODES
		//
	    // NORMAL, ADD, MULTIPLY
	    //
	    // ADDITIONAL MODES:
	    // SCREEN, OVERLAY,
	    // DARKEN, LIGHTEN, COLOR_DODGE, COLOR_BURN
	    // HARD_LIGHT, SOFT_LIGHT, DIFFERENCE,
	    // EXCLUSION, HUE, SATURATION, COLOR, LUMINOSITY
	    //
		const AdditiveBlendMode = engine.BLEND_MODES.ADD;

		this.dustFar.alpha = 0.4;
		this.dustFar.blendMode = AdditiveBlendMode;
		this.dustNear.alpha = 0.3;
		this.dustNear.blendMode = AdditiveBlendMode;
		this.dustNear2.alpha = 0.1;
		this.dustNear2.blendMode = AdditiveBlendMode;


		//ctx.globalCompositeOperation = 'lighter';


		this.container.addChild(this.sprite);
		this.container.addChild(this.texture);
		this.container.addChild(this.dustFar);
		this.container.addChild(this.dustNear);
		this.container.addChild(this.dustNear2);
		this.container.addChild(this.vignette);
		this.container.addChild(this.logo);
		this.container.addChild(this.screen);

		this.stage.addChild(this.container);//.. add all children to container, not to the stage

		//
		// PERFORMANCE CopyTEX
		//
		//stage.filters = [new Parallax.filters.VoidFilter()];
		//stage.filterArea = new Parallax.Rectangle(0, 0, renderer.width, renderer.height);

		//Add the canvas to the HTML document

		let task;
		let request = this.requestFrame('request');
		let cancel = this.requestFrame('cancel');

		this.animation = {

			update: (time)=>{

				task = request(this.animation.update);
				//sprite.position.y += (lFollowY - sprite.position.y) * 1/props.friction;
				this.animation.task.run(time);
				this.TWEEN.update(time);
				renderer.render(this.stage);

			},
			task: {

				add: function(id,task){

					this.pool[id]=task;
				},

				merge: function(id,task){
					var cache = this.pool[id];
					delete this.pool[id];
					this.pool[id]=angular.merge(cache, task);
					console.log('Merge: '+this.pool[id]);
				},
				remove: function(id){
					delete this.pool[id];
				},
				run: function(time){

					angular.forEach(this.pool, function(val, key) {
						//console.log('key:'+ key + ' | val: ' + val);
						val(time);
					});

				},
				pool: {}
			}

		};


		this.process = {

				init: () => {
					if (/*@cc_on!@*/false) { // check for Internet Explorer
						document.onfocusin = this.process.resume;
						document.onfocusout = this.process.suspend;
					} else {
						window.onfocus = this.process.resume;
						window.onblur = this.process.suspend;
					}
				},
				resume: () => {
					console.log('WebGL 2D process resumed');
					this.task = request(this.animation.update);
				},
				suspend: () => {
					console.log('WebGL 2D process suspended');
					cancel(task);
				},


		};

		this.process.init();


		this._element.append(renderer.view);

		//this.renderer.render(this.stage);

		task = request(this.animation.update);

		//
		// Scene FadeIn
		//

		new this.TWEEN.Tween({ alpha: 1, screen: this.screen })
		    .to({alpha:0}, 3000)
		    .on('update', ({alpha}) => {
				this.screen.alpha = alpha;
		    })
			.start();

		//
		// SECTION HANDLING BETA
		//


		this.section = function(value){

			switch(value){

				case '/explore':

					//
					// E X P L O R E
					//

					console.log('explorer <---');

					var SEQ = {
						LOGO_BLUR_OUT: new this.TWEEN.Tween({blur:0})
							.easing(this.TWEEN.Easing.Sinusoidal.Out)
							.to({blur:10}, 500)
							.on('update',({blur})=>{
								this.logo.filter.blur.blur = blur;
								//logo.scale.x += this.blur*0.0008;
								//logo.scale.y += this.blur*0.0008;
								this.logo.x = renderer.width/2-this.logo.width/2;
								this.logo.y = renderer.height/2-this.logo.height/2;
							}),
						LOGO_FADE_OUT: new this.TWEEN.Tween({alpha:1})
							.to({alpha:0}, 500)
							.on('update', ({alpha}) =>{
								if(this.logo.alpha > 0){
									this.logo.alpha = alpha;
								}
								this.texture.alpha = alpha;
							})
							.on('complete', () => {

								this.animation.task.remove('laserbeam');
								//texture.dispose();

							}),
						SPRITE_MOD: new this.TWEEN.Tween(this.TRANSFORMATIONS._inherit.sprite)
						.easing(this.TWEEN.Easing.Sinusoidal.Out)
					    .to(this.TRANSFORMATIONS.explore.sprite, 600)
					    .on('update', ({scaleX, scaleY, rotation, pivotX, pivotY}) => {

						   this.sprite.scale = {x: scaleX, y: scaleY};
					       this.sprite.rotation = rotation;
					       this.sprite.pivot.set(pivotX,pivotY);

					    })

					}
					new this.TWEEN.Tween().to({},0).chain(
						SEQ.LOGO_BLUR_OUT,
						SEQ.LOGO_FADE_OUT.delay(10),
						SEQ.SPRITE_MOD
					)
					.start();

					//sequence.A.chain(sequence.B).start();
					break;

				default:

					//
					// EASING EQUATION:
					//
					// Linear,
					// Quadratic,
					// Cubic,
					// Quartic,
					// Quintic,
					// Sinusoidal,
					// Exponential,
					// Circular,
					// Elastic,
					// Back,
					// Bounce
					//
					// EASING TYPES: In, Out, InOut
					//

					console.log('default count <--');

					var SEQ = {
						SPRITE: new this.TWEEN.Tween(this.TRANSFORMATIONS._inherit.sprite)
						.easing(this.TWEEN.Easing.Quartic.InOut)
					    .to(this.TRANSFORMATIONS._default.sprite, 600)
					    .on('update',({scaleX, scaleY, rotation, pivotX, pivotY}) => {
					       // console.log(this.x, this.y);
						   this.sprite.scale = {x: scaleX, y: scaleY};
					       this.sprite.rotation = rotation;
					       this.sprite.pivot.set(pivotX,pivotY);

					    })
					    .on('start',()=>{
						    //
							// dispose();
							// Frees the texture from WebGL memory
							// without destroying this texture object.
							//
							//texture.cacheAsBitmap = true;

							//animation.task.add('draw2', function(delta){
								//texture.rotation = this.draw.dx * 0.04;
							//});

						}),
						LASERBEAM: new this.TWEEN.Tween(this.TRANSFORMATIONS._inherit.texture)
						.to(this.TRANSFORMATIONS._default.texture,2000)
						.on('start',(delta)=>{


								this.animation.task.add('laserbeam', (delta) =>{
									this.texture.tilePosition.x = delta*0.4;
								});

						})
						.on('update', (delta) => {
							//texture.tilePosition.x = delta;
						}),
						LOGO: new this.TWEEN.Tween({alpha:0,blur:10})
						.to({alpha:1,blur:0})
						.on('update', ({alpha,blur}) =>{
							this.logo.visible = true;
							this.logo.alpha = alpha;


							this.logo.filter.blur.blur = blur;

							if(this.logo.scale.x > 1){
								//logo.scale.x -= this.blur*0.0008;
								//logo.scale.y -= this.blur*0.0008;
								this.logo.x = renderer.width/2-this.logo.width/2;
								this.logo.y = renderer.height/2-this.logo.height/2;
							}

							//shockwave.time = delta;
							if(this.texture.alpha < 1){
								this.texture.alpha = alpha;
							}
						})

					}


					new this.TWEEN.Tween().to({},0).chain(
						SEQ.SPRITE,
						SEQ.LASERBEAM.repeat(Infinity).delay(0),
						SEQ.LOGO,
					).start();

					return;

			}

		};


		this.section(this._location.path());

		$scope.$watch(()=>{

			//thanks google, wtf is this...
			return this._location.path();

		}, (value)=>{

			this.section(value);

		});


		//
		// DEFAULT TASK
		//
		this.animation.task.add('draw', (delta) => {

			//console.log('draw');
			this.modifiers.i += 0.78;
			//var i = this.modifiers.i;

			// Mouse damping
			this.mouse.cx = this.mouse.x * 0.05 + this.mouse.cx * 0.95;
			this.mouse.cy = this.mouse.y * 0.05 + this.mouse.cy * 0.95;


			this.draw.mx = -(this.mouse.cx / renderer.width) * this.modifiers.tumble;
			this.draw.my = -(this.mouse.cy / renderer.height) * this.modifiers.tumble;
			this.draw.x = (renderer.width - this.modifiers.w) / 2 + noise.noise2( this.modifiers.i/193, this.modifiers.i/233)*this.modifiers.tumble + this.draw.mx;
			this.draw.y = 0 +  + noise.noise2( this.modifiers.i/241, this.modifiers.i/211)*this.modifiers.tumble + this.draw.my + this.modifiers.tumble;

			this.sprite.position = {x: this.draw.x, y: this.draw.y-this.modifiers.tumble};

			//
			// Dust Layers
			//

			this.draw.dx = this.draw.x + this.modifiers.i/5,
			this.draw.dy = this.draw.y + this.modifiers.i/15 - document.body.scrollTop/8; // extra vert scroll for dust layers

			this.dustFar.tilePosition = noise.modulate(renderer, this.dustFar.texture, this.draw.dx * 2, this.draw.dy * 2);
			this.dustNear.tilePosition = noise.modulate(renderer, this.dustNear.texture, this.draw.dx * 5, this.draw.dy * 5);
			this.dustNear2.tilePosition = noise.modulate(renderer, this.dustNear.texture, this.draw.dx * 25, this.draw.dy * 25);




			//this.sprite.width = width;
			//this.sprite.height = height;
			//this.sprite.position.x += (lFollowX - this.sprite.position.x) * 1/props.friction;

		});


		//
		// Elegant Shim for requesting AnimationFrame | Timeout
		//
		// ES6: const requestFrame = require('request-frame');
		//



		//animation.task.remove('sprite01');

		//animation.task.run();

		//console.log(animation.task.pool)



		var anim = {
			moveBackground: function () {
				x += (lFollowX - x) * 1/props.friction;
				y += (lFollowY - y) * 1/props.friction;

				var translate = 'translate(' + x + 'px, ' + y + 'px) scale('+props.scale+')';

				$elem.css({
					'-webit-transform': translate,
					'-moz-transform': translate,
					'-ms-transform': translate,
					'-o-transform': translate,
					'transform': translate
				});

				window.requestAnimationFrame(anim.moveBackground);
			}
		};

		window.addEventListener('mousemove click', function(e) {

			var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
			var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
			lFollowX = (20 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
			lFollowY = (10 * lMouseY) / 100;

			this.mouse.x = e.clientX;
			this.mouse.y = e.clientY;

		});

		this.resize = function() {

			let w = window.innerWidth;
			let h = window.innerHeight;

			renderer.view.style.width = w + 'px';
			renderer.view.style.height = h + 'px';
			renderer.resize(w,h);


		};

		window.onresize = (event)=> {
			this.resize();
		};



	}

}

class _ParallaxDirective {

	constructor($interval) {
        this.template = '<div style="color:white">I\'m a directive!</div>';
        this.restrict = '';
        this._scope = {}
        // etc. for the usual config options

        // allows us to use the injected dependencies
        // elsewhere in the directive (e.g. compile or link function)
        this.$interval = $interval;
		return this;
    }

	controller($scope, $element){
		'ngInject';
	}


    move(element) {
        element.css('left', (Math.random() * 500) + 'px');
        element.css('top', (Math.random() * 500) + 'px');
    }

}
//
// 'A' - only matches attribute name
// 'C' - only matches class name
// 'M' - only matches comment
// 'E' - only matches element name
//
let ParallaxDirective = function(){
	'ngInject';
	return {
		template: 'Hello <input ng-model="name"><hr/><span ng-bind="name"></span> <br/> <span ng:bind="name"></span> <br/> <span ng_bind="name"></span> <br/> <span data-ng-bind="name"></span> <br/><span x-ng-bind="name"></span> <br/>',
		restrict: '',
		scope: {
			// parallaxFriction: '@',
			// parallaxScale: '@',
			// parallaxTexture: '@',
		},
		//contoller: ParallaxController
	}
}
let ParallaxDirectiveWrapper = function(WebGLRenderer2D, TWEEN, requestFrame, $location){

	'ngInject';

	let Directive = new RealParallaxDirective(WebGLRenderer2D, TWEEN, requestFrame, $location);
	// Directive.controller = ParallaxController;
	// Directive.link = function(this._scope, $elem){
	// 	console.log(this._scope.customer);
	// }

	return Directive;

}
export default ParallaxDirectiveWrapper;
