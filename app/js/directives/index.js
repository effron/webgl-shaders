//
// DIRECTIVES
//

import angular from 'angular';

let Directives = angular.module('app.directives', ['app.services']);

import ParallaxDirectiveWrapper from './parallax.directive';
Directives.directive('parallax', ParallaxDirectiveWrapper);

export default Directives;
