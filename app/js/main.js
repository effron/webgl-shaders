//
//            █▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀█
//            ▓ ▒░░░▒▒▒▓▓▒▒▒░░░ █
//            ▓ ░░░░░░░▒▒░░░░░░ █
//            ▓ ░▀▀▀░░░░░░░░▒▒▒ ▓
//            ▓ ▒▒░▒▒▒▒░░▒▒▒▓▓▓ ▒
//            ▒ ▒▓▓▓▓▓▓▒▒▓▓▓▓▓▓ ░
//            ░ ▒▒░░▒▓▒░░░▓░▀▀░B·E·C·A·U·S·E·
//  ██╗   ██╗ ██████╗ ░█████╗▐██████╗ ████████╗██╗  ██╗███████╗██╗   ██╗
//  ╚██╗ ██╔╝██╔═══██╗██╔  ██╗██╔══██╗╚══██╔══╝██║  ██║██╔════╝██║   ██║
//   ╚████╔╝ ██║▒░░██║███████║██████╔╝   ██║   ███████║█████╗  ██║   ██║
//    ╚██╔╝  ██║▒░░██║██╔══██║██╔══██╗   ██║   ██╔══██║██╔══╝  ██║   ██║
//     ██║   ╚██████╔╝██║░░██║██║  ██║   ██║   ██║  ██║██║     ╚██████╔╝
//     ██▒    ░▒░▒░░ ▄▒▒▓▄▄▓▒█▒▓░▀ ▒▓░   ▒░░   ▒░░  ░▒ ▒░       ░▒▓▒▒▒
//     ▒ ▒    ▒  ▄▄██▒THE·REALTIME·WEBGL·EXPERIMENT ░  ░        ░░▒░░░
//     ░ ░    ▓ ▓▓▓▓▓▓▓▒▒▓▓▓▓▓▓ ▒   ░     ░    ░░   ░  ░░        ░░░░░
//     ░ ░    ▓ ▓▓▒▓▓▒▒░░▒▒▓▓▒▒ ▓              ░░   ░              ░
//            ▓ ▒▒░▒▒░░░░░░▒▒░░ █
//            █ ░░░░░░░▒▒░░░░░░ █
//            █ ░░▒░░▒▒▓▓▒▒░░▒▒ █
//            █ ▒▒▓▒▒▓▓██▓▓▒▒▓▓ █
//            █▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄█
//


require.config({
    baseUrl: "assets/js",
    paths: {
        //loadouts
        
        'domReady': 'vendor/domReady',
        'lodash': 'vendor/lodash',
        'angular': 'vendor/angular',
		'angular-route':'vendor/angular-route',
		'angular-animate':'vendor/angular-animate',
		'angular-aria':'vendor/angular-aria',
		'angular-material':'vendor/angular-material',
		'angular-messages':'vendor/angular-messages',
		'angular-sanitize':'vendor/angular-sanitize',
		'jquery': 'vendor/jquery.min',
		'jquery-ui': 'vendor/jquery-ui',
        '_bootstrap': 'vendor/bootstrap',
		'threejs': 'vendor/three.min',
		//'webgl-widget': 'vendor/webgl-widget',
		'pixi': 'vendor/pixi.min',
		'pixi-filters': 'vendor/pixi-filters.min',
		'pixi-picture': 'vendor/pixi-picture',
		'request-frame': 'vendor/request-frame',
		'webfontloader': 'vendor/webfontloader',
		'tween': 'vendor/Tween',
		'smooth-scrollbar': 'vendor/smooth-scrollbar',
		//'es6-shim': '/bower_components/es6-shim/es6-shim.min',		
        
        //'angularAMD': '/bower_components/angularAMD/angularAMD.min',
        //'ngload': '/bower_components/angularAMD/ngload.min',
    
    },
    shim: {
        //'angularAMD': ['angular'],
        //'ngload': ['angularAMD']
		//'angular': { exports: 'angular' }    
		'angular': {
			init: function(){
				
			},
			exports:'angular',
			
		},
		'_bootstrap': {deps:['jquery']},
		'angular-route': {deps:['angular']},
		'angular-animate': {deps:['angular']},
		'angular-aria': {deps:['angular']},
		'angular-messages': {deps:['angular']},
		'angular-sanitize': {deps:['angular']},
		'angular-material': {deps:['angular-animate','angular-aria','angular-messages', 'angular-sanitize']},
		'pixi-filters': {deps:['pixi']},
		'pixi-picture': {deps:['pixi']}
    },    
    deps: [
		'bootstrap',//load boostrap startup file
    	//'/bower_components/jquery/dist/jquery.min.js',
    	//'/bower_components/bootstrap/dist/js/bootstrap.min.js'
		//'components/intro/intro.module',
		//'components/intro/intro.component'
	]
});