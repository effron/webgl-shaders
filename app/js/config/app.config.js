function AppConfig($httpProvider, $routeProvider, $locationProvider) {
  'ngInject';

	/*
	  If you don't want hashbang routing, uncomment this line.
	  Our tutorial will be using hashbang routing though :)
	*/
	// $locationProvider.html5Mode(true);

	$locationProvider.hashPrefix('!');

	//$locationProvider.html5Mode(true);
	$routeProvider
	.when('/', {
		templateUrl: 'views/main.html',
		controller: function(){
			console.log('intro');
			//console.log(Parallax);
		}
	})
    .when('/explore', {
		templateUrl: 'views/explore.html',
		controller: function(){
			//console.log(Parallax);
		}
	})
    .when('/test', {
		templateUrl: 'views/test.html',
		controller: function(){

            document.domain = "daftpunk.local";

            var doc = document.querySelector('.doc');
            var info = document.querySelector('.info');
            //console.dir(doc);

            document.addEventListener("DOMContentLoaded", function(e) {
              doc.addEventListener('load', function(e) {
//                console.dir(doc.contentWindow.document.body.querySelector('.dummy'));

                var prop1 = doc.contentWindow.document.body.querySelector('.dummy');
                var prop2 = doc.contentWindow.document.body.querySelector('embed');

                info.innerText = prop1 + ', ' + prop2.src;

              }, false);
            });

		}
	})
    .when('/pdf', {
        templateUrl: 'views/pdf.html',
        controller: function(){
            document.domain = "daftpunk.local";

        }
    })
	.otherwise('/');//if no matches

}

export default AppConfig;
