const AppConstants = {
  api: 'https://localhost:8000/api',
  appName: 'Portfolio II',
};

export default AppConstants;
