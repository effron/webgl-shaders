uniform sampler2D tDiffuse; 
uniform float time; 
uniform float strength; 
uniform float size; 
uniform float speed; 
varying vec2 vUv; 
void main() {
	vec2 p = -1.0 + 2.0 * vUv; 
	gl_FragColor = texture2D(tDiffuse, vUv + strength * vec2(cos(time*speed+length(p*size)), sin(time*speed+length(p*size)))); 
	gl_FragColor.a = (gl_FragColor.a * 0.01);
	gl_FragColor.a = gl_FragColor.a + 0.5;
}